#ifndef IREAL_H
#define IREAL_H

#include <cmath>


namespace IEngine
{

/* --- Constants --- */

#ifdef GS_REAL_DOUBLE

using Real = double;

#else

using Real = double;

#endif

}


#endif // IREAL_H
