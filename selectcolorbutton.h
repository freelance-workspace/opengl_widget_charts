#ifndef SELECTCOLORBUTTON_H
#define SELECTCOLORBUTTON_H

#include <QPushButton>
#include <QColor>

/**
 * @brief The SelectColorButton class
 * a class for choosing a color
 *  using the selection color dialog
 */
class SelectColorButton : public QPushButton
{
    Q_OBJECT
public:

    /// \brief SelectColorButton - Default constructor
    /// \param parent
    SelectColorButton( QWidget* parent );

    void setColor( const QColor& color );
    const QColor& getColor();

public slots:
    void updateColor();
    void changeColor();

private:

    QColor color;//< color 4d RGBA
};

#endif // SELECTCOLORBUTTON_H
