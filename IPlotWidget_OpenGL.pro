QT += core gui network opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = engine_OpenGL
TEMPLATE = app


#INCLUDEPATH += \                         # <-
#    include \                            # <- Without these 3 lines the compile fails
#    ../glm              # <-

#QMAKE_CXXFLAGS += -m32
CONFIG += c++11
CONFIG += resources_big

DEFINES += ENABLE_STL_SUPPORT

#Linux
linux: {

#Android
 android: {
  LIBS +=  -lGLESv1_CM -lGLESv2
}

#Linux default
 !android: {
   LIBS += -lGL -lGLU #-lglut #-lGLEW
}

}

#Windows
win32: {
   LIBS += -lopengl32 -lglu32 #  -lglew32
}

#Windows
win64: {
   LIBS += -lopengl32 -lglu32 # -lglew32

}

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Engine/IQCamera.cpp \
    Engine/ISegments/ILine3D.cpp \
    Engine/ISegments/ILineSegment3D.cpp \
    Engine/ISegments/IRay.cpp \
    WidgetPlot/GLWidgetPlot.cpp \
    WidgetPlot/IPlotFunctionGraph2D.cpp \
    WidgetPlot/SceneCore.cpp \
    main.cpp \
    mainwindow.cpp \
    WidgetPlot/Shader/Shader.cpp \
    selectcolorbutton.cpp

HEADERS += \
    Engine/IMath/IAffineTransform.h \
    Engine/IMath/IAlgebra.h \
    Engine/IMath/IComplex.h \
    Engine/IMath/IFunc.h \
    Engine/IMath/IMaths.h \
    Engine/IMath/IMatrix.h \
    Engine/IMath/IMatrix2x2.h \
    Engine/IMath/IMatrix3x3.h \
    Engine/IMath/IMatrix4x4.h \
    Engine/IMath/IPlane.h \
    Engine/IMath/IQuaternion.h \
    Engine/IMath/IRay.h \
    Engine/IMath/IReal.h \
    Engine/IMath/IScalarType.h \
    Engine/IMath/ISpherical.h \
    Engine/IMath/ITransform.h \
    Engine/IMath/IVector.h \
    Engine/IMath/IVector2D.h \
    Engine/IMath/IVector3D.h \
    Engine/IMath/IVector4D.h \
    Engine/IMath/IVectorType.h \
    Engine/IMath/imaths.h \
    Engine/IQCamera.h \
    Engine/ISegments/ILine3D.h \
    Engine/ISegments/ILineSegment3D.h \
    Engine/ISegments/IRay.h \
    Engine/imaths.hpp \
    WidgetPlot/GLWidgetPlot.h \
    WidgetPlot/IPlotFunctionGraph2D.h \
    WidgetPlot/SceneCore.h \
    mainwindow.h \
    WidgetPlot/Shader/Shader.h \
    selectcolorbutton.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \

RESOURCES += \
    shaders.qrc
