#version 430

//attribute vec4 qt_Vertex;
//attribute vec4 qt_MultiTexCoord0;
//uniform mat4 qt_ModelViewProjectionMatrix;
//varying vec4 qt_TexCoord0;

layout(std430, binding = 0) buffer TVertex {
   vec4 vertex[];
};

uniform mat4  u_mvp;
uniform vec2  u_resolution;
uniform float u_thickness;

uniform bool u_isRenderPoints;

//varying highp vec4 v_color;

flat out vec3 startPos;
out vec3 vertPos;

void main(void)
{

    int line_i = gl_VertexID / 12;
    int tri_i  = gl_VertexID % 12;

    vec4 va[8];
    for (int i=0; i<8; ++i)
    {
        va[i] = u_mvp * vertex[line_i+i];
        va[i].xyz /= va[i].w;
        va[i].xy = (va[i].xy + 1.0) * 0.5 * u_resolution;
    }

    vec2 v_line  = normalize(va[2].xy - va[1].xy);
    vec2 nv_line = vec2(-v_line.y, v_line.x);

    vec2 v_line2  = normalize(va[2].xy - va[3].xy);
    vec2 nv_line2 = vec2(-v_line2.y, v_line2.x);

    vec4 pos;
    if (tri_i == 0 || tri_i == 1 || tri_i == 3)
    {
        vec2 v_pred  = normalize(va[1].xy - va[0].xy);
        vec2 v_miter = normalize(nv_line);

        pos = va[1];
        pos.xy += v_miter * u_thickness * (tri_i == 1 ? -0.5 : 0.5) / dot(v_miter, nv_line);

    }
    else if (tri_i == 2 || tri_i == 4 || tri_i == 5)
    {
        vec2 v_succ  = normalize(va[3].xy - va[2].xy);
        vec2 v_miter = normalize(nv_line);

        pos = va[2];
        pos.xy += v_miter * u_thickness * (tri_i == 5 ? 0.5 : -0.5) / dot(v_miter, nv_line);

    }
    else
    {
        if(tri_i == 10 || tri_i == 7 )
        {
            vec2 v_miter = normalize(nv_line2);
            pos = va[2];
            pos.xy += v_miter * u_thickness *  (tri_i == 7 ? 0.5 : -0.5) / dot(v_miter, nv_line2);
        }
        else if(tri_i == 11 || tri_i == 6)
        {
            vec2 v_miter = normalize(nv_line);
            pos = va[2];
            pos.xy += v_miter * u_thickness * (tri_i == 11 ? 0.5 : -0.5) / dot(v_miter, nv_line);
        }
        else
        {
            pos = va[2];
        }
    }

    pos.xy = pos.xy / u_resolution * 2.0 - 1.0;
    pos.xyz *= pos.w;
    gl_Position = pos;
    vertPos     = pos.xyz / pos.w;
    startPos    = vertPos;

}
