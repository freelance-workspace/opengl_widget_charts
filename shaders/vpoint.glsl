#version 450

//layout(location=0) in vec4 vertexPosition;

attribute vec4 vertexPosition;

uniform mat4  u_mvp;
uniform float  u_pointSize;

flat out vec3 startPos;
out vec3 vertPos;

void main(void){

    vec4 pos = u_mvp * vertexPosition;

    gl_PointSize = u_pointSize;
    gl_Position = pos;

    vertPos     = pos.xyz / pos.w;
    startPos    = vertPos;

}
