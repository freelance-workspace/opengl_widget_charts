#version 400

uniform vec2 u_resolution;
uniform vec4 u_color;

flat in vec3 startPos;
in vec3 vertPos;

void main(void)
{
       //color 4d
      gl_FragColor = u_color;
}
