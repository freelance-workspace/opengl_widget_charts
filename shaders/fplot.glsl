#version 430

//uniform vec4 u_color;#version 430

uniform vec4 u_color;
uniform bool u_isDashed;

uniform float u_dashSize;
uniform float u_gapSize;

uniform vec2 u_resolution;

//varying highp vec4 v_color;

flat in vec3 startPos;
in vec3 vertPos;

void main()
{
    if(u_isDashed)
    {
        vec2  dir  = (vertPos.xy-startPos.xy) * u_resolution/2.0;
        float dist = length(dir);

        if (fract(dist / (u_dashSize + u_gapSize)) > u_dashSize/(u_dashSize + u_gapSize))
        {
                discard;
        }
    }

    gl_FragColor = vec4(u_color.rgb,1.0);
}
