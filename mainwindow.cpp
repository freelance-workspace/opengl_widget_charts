#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QLineEdit>
#include <QLabel>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Graphics Function Widget");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    ui->widget_2->keyPressEvent(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    ui->widget_2->keyReleaseEvent(event);
}


void MainWindow::on_horizontalSlider_2_sliderMoved(int position)
{
    qDebug() << position;
    ui->widget_2->scene()->graphPlots()->setWidthLine(position);
}


void MainWindow::on_checkBox_toggled(bool checked)
{
    ui->widget_2->scene()->graphPlots()->setIsFill(checked);
}


void MainWindow::on_pushButton_clicked()
{
    qDebug() << ui->pushButton->getColor();

    Vector4 color(
    ui->pushButton->getColor().redF(),
    ui->pushButton->getColor().greenF(),
    ui->pushButton->getColor().blueF(),
    1.0);
    ui->widget_2->scene()->graphPlots()->setColor(color);

}


void MainWindow::on_horizontalSlider_3_valueChanged(int value)
{
    float v = value / 10.f;
    qDebug() << v;
    ui->widget_2->scene()->graphPlots()->setScale_X( v );
}


void MainWindow::on_verticalSlider_valueChanged(int value)
{
    qDebug() << value/50.f;
    ui->widget_2->scene()->graphPlots()->setScale_Y( value/10.f );
}


void MainWindow::on_pushButton_2_clicked()
{
    ui->widget_2->graphPlots()->setType(Orintation_Plot::HORIZONTAL);
    ui->widget_2->scene()->setIsEnableScrollingX(true);
    ui->widget_2->scene()->setIsEnableScrollingY(true);
    ui->widget_2->scene()->setIsEnableZoom(true);
    ui->widget_2->scene()->setZoom(2);
    ui->widget_2->camera().SetOrthographic(false);
    ui->widget_2->graphPlots()->setIsOptimization(true);

    float dt = 1.f/50.f;
    ui->widget_2->graphPlots()->setStepDifferent(dt);
    ui->widget_2->graphPlots()->points().clear();

    // initilization graphyics random points
    for (int i = -6000000/2,j=0; i < 6000000/2; ++i,++j)
    {
       float x = cos(i * 1.f/10.f) * ((i%1==0)? float(1+rand()%100)/200.f : 1.f);
       float y = i * 1.f/50.f;///100.f;
       ui->widget_2->graphPlots()->AppendPoint(Vector2(y,x));
       //mGraphPlots->AppendPoint(Vector2(y,x) + Vector2(0.001,0));
    }
}


void MainWindow::on_checkBox_Dashed_toggled(bool checked)
{
    ui->widget_2->graphPlots()->setIsDashed(checked);
}


void MainWindow::on_checkBox_DrawPoints_toggled(bool checked)
{
   ui->widget_2->graphPlots()->setIsDrawPoint(checked);
}

