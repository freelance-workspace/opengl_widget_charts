#ifndef IPLOTFUNCTIONGRAPH2D_H
#define IPLOTFUNCTIONGRAPH2D_H

#include <QObject>
#include <QVector>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions_4_3_Compatibility>
#include "../Engine/imaths.hpp"
#include "../Engine/IQCamera.h"
#include "../WidgetPlot/Shader/Shader.h"

#include <GL/gl.h>


using namespace IEngine;


/// Orientation of graph space
enum struct Orintation_Plot { HORIZONTAL , VERTICAL };

/**
 * \authors werasaimon
 * \version 0.01
 * @brief The IPlotFunctionGraph class
 * displaying graphs on the screen using OpenGL!
 * Opngl - uses a GPU processor
 */
class IPlotFunctionGraph
{

private:

    /// variable to indicate
    /// the orientation of the graph space
    Orintation_Plot mType;

    ///two-dimensional coplanar plane
    /// on which a graph is drawn
    IPlaneCoplanar<float> mPlane;

    ///buffer for storing points in GPU memory
    QOpenGLBuffer    mBufferVBOs;
    QOpenGLBuffer    mBufferVBOsPoint;

    ///buffer for storing points in CPU memory
    QVector<Vector4> mPoints;

    bool    mIsFill; //<enable filling chart
    bool    mIsDashed; //<enable dashed line chart
    bool    mIsDrawPoint; //<enable dashed point chart
    bool    mIsOptimization; //<enable optimization chart

    float   mDashSize; //<Dashed size
    float   mGapSize; //<Dashed gap size

    ///Differential step Dt
    /// \warning the schedule should be set over
    /// \warning the entire area with constant steps Dt
    float   mStepDifferent;

    float   mWidthLine; //< graph lines width
    Matrix4 mModelMatrix;//< plot model matrix
    Vector4 mColor; //< graph color in space colors RGBA
    Vector3 mScale; //< plot scalar along all axes

    //-------------------------//



public:

    /**
     * @brief IPlotFunctionGraph
     * @param OrintationType - sets the orientation, the axis along which the graph is plotted
     * @param n - normal of the coplanar plane on which we draw the graph
     * @param p - any point on a coplanar plane
     */
    IPlotFunctionGraph( const Orintation_Plot& OrintationType = Orintation_Plot::HORIZONTAL ,
                        const IVector3D<float> &n = IVector3D<float>::Z,
                        const IVector3D<float> &p = IVector3D<float>::ZERO)
      : mType(OrintationType),
        mPlane(n,p)
    {
        ///Default values
        mModelMatrix.SetToIdentity();
        mScale.SetAllValues(1,1,1);
        mColor.SetAllValues(1,1,1,1);
        mIsFill=true;
        mIsDashed=false;
        mIsDrawPoint=false;
        mIsOptimization=true;
        mDashSize=10;
        mGapSize=10;
        mBufferVBOs.create();
        mBufferVBOsPoint.create();
        mPoints.resize(1);
    }

    ~IPlotFunctionGraph()
    {
        ///remove points from GPU memory
        mBufferVBOs.destroy();
        mBufferVBOsPoint.destroy();
    }

    ///
    /// \brief ConvertPointIn3DSpace - converts a point from a
    /// two-dimensional space to a three-dimensional
    /// point on the surface of a coplanar plane
    /// \param p - point in 2D
    /// \return point on a 3D surface in a coplanar plane
    const Vector4& ConvertPointIn3DSpace( const Vector2& p) const
    {
        return Vector4(mPlane.ConvertPointIn3DSpace(p),1.0f);
    }


    ///
    /// \brief AppendPoint - add a point to the chart buffer
    /// \param p - point Vector2(x,y) on the graph
    void AppendPoint( const Vector2 &p )
    {
       Vector4 cp = Vector4(mPlane.ConvertPointIn3DSpace(p),1.0);
       mPoints.append( cp );
    }


    /// \brief AddVertexes - add many points to the graph buffer at once
    /// \param points - std::vector<...> 2D point array
    void AddVertexes( const std::vector<Vector2> &points )
    {
        for (unsigned int i = 0; i < points.size(); ++i)
        {
            AppendPoint(points[i]);
        }
    }



    /// \brief RenderGL_Points -a function for rendering OpenGL a points of a scene
    /// \param _camera - camera to view the scene
    /// \param _width - QWidget Width
    /// \param _height - QWidget Width
    //void RenderGL_Points(IQCamera _camera , float _width , float _height);


    /// \brief RenderGL_Points -a function for rendering OpenGL-Shader a points of a scene
    /// \param _camera - camera to view the scene
    /// \param _width - QWidget Width
    /// \param _height - QWidget Width
    /// \param pShader - Program Shader
    /// \param ogl - OpenGL Functions
    void RenderGL_Points(IQCamera _camera , float _width , float _height,
                         GLShaderProgram *pShader,
                         QOpenGLFunctions_4_3_Compatibility *ogl);


    /// \brief RenderShader - a function for rendering OpenGL a graph of a scene using a shader
    /// \param _camera - camera to view the scene
    /// \param _width - QWidget Width
    /// \param _height - QWidget Width
    /// \param pShader - Program Shader
    /// \param ogl - OpenGL Functions
    void RenderShader(IQCamera _camera , float _width , float _height,
                      GLShaderProgram *pShader,
                      QOpenGLFunctions_4_3_Compatibility* ogl);

    //--------------------------------------------//
    /// Update timer
    void Update();
    //--------------------------------------------//

    /// \brief setType
    /// \param newType - variable to indicate
    /// the orientation of the graph space
    void setType(Orintation_Plot newType);

    /// \param enable filling chart
    void setIsFill(bool newIsFill);


    /// \brief scale plot scalar along all axes
    /// \return Vector3 along all axes
    const Vector3 &scale() const;

    /// \param newWidthLine - graph lines width
    void setWidthLine(float newWidthLine);

    /// \brief modelMatrix
    /// \return plot model matrix4x4
    const Matrix4 &modelMatrix() const;

    /// \brief setModelMatrix
    /// \param newModelMatrix - plot model matrix4x4
    void setModelMatrix(const Matrix4 &newModelMatrix);


    /// \brief color graph color in space colors RGBA
    /// \return Vector4(R,G,B,A)
    const Vector4 &color() const;

    ///
    /// \brief setColor graph color in space colors RGBA
    /// \param newColor Vector4(R,G,B,A)
    void setColor(const Vector4 &newColor);


    /// \brief setScale_X scaling the graph along the x-axis
    /// \param scale - size
    void setScale_X(float scale);

    /// \brief setScale_Y - scaling the graph along the x-axis
    /// \param scale - size
    void setScale_Y(float scale);


    /// \brief bufferVBOs buffer for storing points in GPU memory
    /// \return  buffer GPU QOpenGLBuffer
    const QOpenGLBuffer &bufferVBOs() const;

    ///Differential step Dt
    /// \warning the schedule should be set over
    /// \warning the entire area with constant steps Dt
    void setStepDifferent(float newStepDifferent);

    /// \brief type variable to indicate the orientation of the graph space
    /// \return Enum Orintation_Plot
    Orintation_Plot type() const;

    /// \brief points - an array of points in three-dimensional space
    /// \return Vector4
    QVector<Vector4> &points();


    ///is dashed line chart
    bool isDashed() const;


    /// \brief setIsDashed - enable dashed line chart
    /// \param newIsDashed - on/off
    void setIsDashed(bool newIsDashed);

    /// \brief setDashSize
    /// \param newDashSize - Dashed size
    void setDashSize(float newDashSize);

    /// \brief setGapSize
    /// \param newGapSize - Dashed gap size
    void setGapSize(float newGapSize);


    /// \brief setIsDrawPoint - Enable dashed point chart
    /// \param newIsDrawPoint - on/off
    void setIsDrawPoint(bool newIsDrawPoint);

    /// \brief setIsOptimization - Enable optimization chart
    /// \param newIsOptimization - on/off
    void setIsOptimization(bool newIsOptimization);
    bool isDrawPoint() const;
};

#endif // IPLOTFUNCTIONGRAPH2D_H
