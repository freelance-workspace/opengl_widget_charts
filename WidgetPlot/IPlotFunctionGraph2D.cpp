#include "IPlotFunctionGraph2D.h"



bool IPlotFunctionGraph::isDrawPoint() const
{
    return mIsDrawPoint;
}

//void IPlotFunctionGraph::RenderGL_Points(IQCamera _camera, float _width, float _height)
//{
//    glViewport(0, 0, _width , _height);


//    glEnable(GL_PROGRAM_POINT_SIZE);

//    GLfloat sizeRange[2] = {0.0f};
//    glGetFloatv(GL_POINT_SIZE_RANGE, sizeRange);


//    glLoadIdentity();
//    glMatrixMode(GL_PROJECTION);
//    glLoadMatrixf(_camera.ProjectionMatrix());

//    glMatrixMode(GL_MODELVIEW);
//    glLoadMatrixf(_camera.ViewMatrix());

//    glLineWidth(mWidthLine);
//    glPointSize(mWidthLine*0.88f);
//    glPushMatrix();

//    glMultMatrixf(mModelMatrix);

//    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
//    glEnable(GL_BLEND);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glEnable(GL_LINE_SMOOTH);



//     if(mIsOptimization == false)
//     {
//         glPushMatrix();
//         glColor4fv(mColor);
//         for(int i = 0; i < mPoints.size(); ++i)
//         {
//             glBegin(GL_POINTS);
//             glVertex3fv(mPoints[i].GetXYZ());
//             glEnd();
//         }
//         glPopMatrix();
//     }
//     else
//     {
//         Vector3 Axis = (mType == Orintation_Plot::HORIZONTAL) ? Vector3::X : Vector3::Y;

//         int Max_Displacement_Indexes = 100;
//         float L = ((mPoints[0].GetXYZ() - _camera.GetEye()*mModelMatrix.Inverse())).Dot(Axis);
//         int I = (int)(L / (mStepDifferent));
//         int d_sie = (IAbs(_camera.GetEye().z) * Max_Displacement_Indexes * (1.0/mScale.Dot(Axis)));

//         glPushMatrix();
//         glColor4fv(mColor);
//         for(int i = -(100 + d_sie); i < (100 + d_sie); ++i)
//         {
//             int index = I+i;
//             if(index > 0 && index < mPoints.size()
//                    && _camera.FrustumCullingAABB(mPoints[index].GetXYZ()*mModelMatrix,
//                                                  mPoints[index].GetXYZ()*mModelMatrix)
//                     )
//             {
//                 glBegin(GL_POINTS);
//                 glVertex3fv(mPoints[index].GetXYZ());
//                 glEnd();
//             }
//         }
//         glPopMatrix();
//     }

//    glLineWidth(1.0);
//    glDisable(GL_BLEND);
//    glDisable(GL_LINE_SMOOTH);
//}

void IPlotFunctionGraph::RenderGL_Points(IQCamera _camera, float _width, float _height,
                                         GLShaderProgram *pShader,
                                          QOpenGLFunctions_4_3_Compatibility *ogl)
{
    glViewport(0, 0, _width , _height);

    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_BLEND);


//    if(mWidthLine <= 20)
//    {
        glEnable(GL_POINT_SMOOTH);
        glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
//    }
//    else
//    {
//       glDisable(GL_POINT_SMOOTH);
//    }


    glViewport(0, 0, _width , _height);
    glPolygonMode(GL_FRONT_AND_BACK, (mIsFill)?GL_FILL:GL_LINE);

    mModelMatrix = Matrix4::CreateTranslation(_camera.GetEye()) *
                   Matrix4::CreateScale(mScale.x,mScale.y,1) *
                   Matrix4::CreateTranslation(-_camera.GetEye());

    pShader->bind();
    pShader->UniformValue("u_mvp" , _camera.ProjectionViewMatrix() * mModelMatrix);
    pShader->UniformValue("u_resolution" , Vector2( _width, _height) );
    pShader->UniformValue("u_color"      , mColor);
    pShader->UniformValue("u_pointSize"  , ((mWidthLine < 20)? mWidthLine : mWidthLine * 0.9f));



    QVector<Vector4> buffer;
    int size = 0;
    if(mIsOptimization == false)
    {
        buffer.resize(mPoints.size());
        std::memcpy(buffer.data(), mPoints.constData(), mPoints.size() * sizeof(Vector4));
        size = mPoints.size();
    }
    else
    {
        Vector3 Axis = (mType == Orintation_Plot::HORIZONTAL) ? Vector3::X : Vector3::Y;

        int Max_Displacement_Indexes = 100;
        float L = ((mPoints[0].GetXYZ() - _camera.GetEye()*mModelMatrix.Inverse())).Dot(Axis);
        int I = (int)(L / (mStepDifferent));
        int d_sie = (IAbs(_camera.GetEye().z) * Max_Displacement_Indexes * (1.0/mScale.Dot(Axis)));

        Vector2 old_point;
        for(int i = -(100 + d_sie); i < (100 + d_sie); ++i)
        {
            int index = I+i;
            if(index > 0 && index < mPoints.size())
            {
                buffer.push_back( mPoints[index] );
            }
        }
    }

    pShader->enableAttributeArray("vertexPosition");
    pShader->setAttributeArray("vertexPosition", (QVector4D*)buffer.constData());

    glDrawArrays(GL_POINTS, 0, buffer.size());

    pShader->release();
}


void IPlotFunctionGraph::RenderShader(IQCamera _camera, float _width, float _height,
                                      GLShaderProgram *pShader,
                                      QOpenGLFunctions_4_3_Compatibility *ogl)
{

    glViewport(0, 0, _width , _height);
    glPolygonMode(GL_FRONT_AND_BACK, (mIsFill)?GL_FILL:GL_LINE);

   // glEnable(GL_LINE_SMOOTH);
   // glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);


    glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    //    AffineTransform AffineScaleToPlot;
    //    AffineScaleToPlot.ScaleAroundLocalPoint( Vector3::X , m_Scale.x , _camera.GetEye());
    //    mModelMatrix = AffineScaleToPlot.GetTransformMatrix();

    mModelMatrix = Matrix4::CreateTranslation(_camera.GetEye()) *
                   Matrix4::CreateScale(mScale.x,mScale.y,1) *
                   Matrix4::CreateTranslation(-_camera.GetEye());


   // if(mIsDrawPoint==true) RenderGL_Points(_camera,_width,_height);


    pShader->bind();

    pShader->UniformValue("u_mvp" , _camera.ProjectionViewMatrix() * mModelMatrix);
    pShader->UniformValue("u_resolution" , Vector2( _width, _height) );
    pShader->UniformValue("u_thickness"  , mWidthLine);
    pShader->UniformValue("u_color"      , mColor);
    pShader->UniformValue("u_isDashed"   , mIsDashed);
    pShader->UniformValue("u_dashSize"   , mDashSize);
    pShader->UniformValue("u_gapSize"    , mGapSize);
    pShader->UniformValue("u_isRenderPoints" , mIsDrawPoint);



    mBufferVBOs.bind();
    mBufferVBOs.allocate(mPoints.size() * sizeof(Vector4));

    Vector4* buffer_data = (Vector4*)mBufferVBOs.map(QOpenGLBuffer::WriteOnly);

    int size = 0;
    if(mIsOptimization == false)
    {
        std::memcpy(buffer_data, mPoints.constData(), mPoints.size() * sizeof(Vector4));
        size = mPoints.size();
    }
    else
    {
        if (buffer_data != (Vector4*) NULL)
        {
            Vector3 Axis = (mType == Orintation_Plot::HORIZONTAL) ? Vector3::X : Vector3::Y;

            int Max_Displacement_Indexes = 100;
            float L = ((mPoints[0].GetXYZ() - _camera.GetEye()*mModelMatrix.Inverse())).Dot(Axis);
            int I = (int)(L / (mStepDifferent));
            int d_sie = (IAbs(_camera.GetEye().z) * Max_Displacement_Indexes * (1.0/mScale.Dot(Axis)));

            Vector2 old_point;
            for(int i = -(100 + d_sie); i < (100 + d_sie); ++i)
            {
                int index = I+i;
                if(index > 0 && index < mPoints.size())
                {
                    buffer_data[size++] = mPoints[index];

                    /**/
                    if(mIsDrawPoint != true)
                    {
                        float epsilon = 0.002;
                        Vector2 eps_shift = (mType == Orintation_Plot::HORIZONTAL) ? Vector2(epsilon,0) : Vector2(0,epsilon);
                        Vector2 p = mPlane.ConvertPointInPlane2D( mPoints[index].GetXYZ() );
                        if( i == -(100 + d_sie))
                        {
                            buffer_data[size++]=Vector4(mPlane.ConvertPointIn3DSpace(p + eps_shift),1.0);
                        }
                        else if(index > 1 && index < mPoints.size()-2)
                        {
                            float angle = (p-old_point).AngleBetween(p-mPlane.ConvertPointInPlane2D( mPoints[index+1].GetXYZ()));
                            if(angle < M_PI / 3.f)
                            {
                               // buffer_data[size++]=Vector4(mPlane.ConvertPointIn3DSpace(p + eps_shift * epsilon),1.0);
                            }
                        }
                        old_point = p;
                    }
                    /**/
                }

            }
        }
    }



    ogl->glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, mBufferVBOs.bufferId());
    GLsizei N2 = (GLsizei)size-2;
    glDrawArrays(GL_TRIANGLES, 0, 12*(N2-1));

    mBufferVBOs.unmap();
    mBufferVBOs.release();

    pShader->release();
}

void IPlotFunctionGraph::Update()
{
   // GLfloat sizeRange[2] = {0.0f};
   // glGetFloatv(GL_POINT_SIZE_RANGE, sizeRange);
   // mWidthLine=IClamp(mWidthLine,sizeRange[0],sizeRange[1]);
}


void IPlotFunctionGraph::setType(Orintation_Plot newType)
{
    mType = newType;
}

void IPlotFunctionGraph::setIsFill(bool newIsFill)
{
    mIsFill = newIsFill;
}

const Vector3 &IPlotFunctionGraph::scale() const
{
    return mScale;
}

void IPlotFunctionGraph::setWidthLine(float newWidthLine)
{
    mWidthLine = newWidthLine;
}

const Matrix4 &IPlotFunctionGraph::modelMatrix() const
{
    return mModelMatrix;
}

void IPlotFunctionGraph::setModelMatrix(const Matrix4 &newModelMatrix)
{
    mModelMatrix = newModelMatrix;
}

const Vector4 &IPlotFunctionGraph::color() const
{
    return mColor;
}

void IPlotFunctionGraph::setColor(const Vector4 &newColor)
{
    mColor = newColor;
}

void IPlotFunctionGraph::setScale_X(float scale)
{
    mScale.x = scale;
}

void IPlotFunctionGraph::setScale_Y(float scale)
{
    mScale.y = scale;
}

const QOpenGLBuffer &IPlotFunctionGraph::bufferVBOs() const
{
    return mBufferVBOs;
}

void IPlotFunctionGraph::setStepDifferent(float newStepDifferent)
{
    mStepDifferent = newStepDifferent;
}

Orintation_Plot IPlotFunctionGraph::type() const
{
    return mType;
}


QVector<Vector4> &IPlotFunctionGraph::points()
{
    return mPoints;
}

bool IPlotFunctionGraph::isDashed() const
{
    return mIsDashed;
}

void IPlotFunctionGraph::setIsDashed(bool newIsDashed)
{
    mIsDashed = newIsDashed;
}

void IPlotFunctionGraph::setDashSize(float newDashSize)
{
    mDashSize = newDashSize;
}

void IPlotFunctionGraph::setGapSize(float newGapSize)
{
    mGapSize = newGapSize;
}

void IPlotFunctionGraph::setIsDrawPoint(bool newIsDrawPoint)
{
    mIsDrawPoint = newIsDrawPoint;
}

void IPlotFunctionGraph::setIsOptimization(bool newIsOptimization)
{
    mIsOptimization=newIsOptimization;
}
